<?php
$_['text_button_wishlist'] = 'Wish list';
$_['text_button_compare'] = 'Compare';
$_['text_recently_added'] = 'Recently added item(s)';
$_['text_submit_comment'] = 'Post Your Comment';
$_['text_read_more'] = 'Read more';
$_['text_product_details'] = 'Product Details';
$_['text_related_products'] = 'YOU MAY ALSO LIKE';
$_['button_cart_special'] = 'Buy product';