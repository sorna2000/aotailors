<style>
    .option-selection > label {
        width: 100%;
    }    
</style>

<?php

$front_neck = "Front-Neck";
$back_neck = "Back Neck";
$sleeve = "Sleeve";
$front_neck = "Front-Neck";


$kuler = Kuler::getInstance();
$theme = $kuler->getTheme();
$kuler->addScript(array(
  "catalog/view/theme/$theme/js/lib/jquery.elevatezoom.js",
  "catalog/view/theme/$theme/js/product.js"
), true);
$kuler->language->load('kuler/neon');
global $config;
?>
<?php echo $header; ?>
  <div class="container">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-9 col-sm-9'; ?>
      <?php } else { ?>
        <?php $class = 'col-lg-12 col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="row">
          <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-lg-6 col-sm-6'; ?>
          <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-lg-5 col-sm-5'; ?>
          <?php } else { ?>
            <?php $class = 'col-lg-5 col-sm-5'; ?>
          <?php } ?>
          <div class="<?php echo $class; ?>">
            <?php if ($thumb || $images) { ?>
              <ul class="thumbnails">
                <?php if ($thumb) { ?>
                  <div class="thumbnails__big-image" style="max-width: <?php echo $config->get('config_image_thumb_width'); ?>px; max-height: <?php echo $config->get('config_image_thumb_height'); ?>px;">
                    <a href="<?php echo $popup; ?>" class="product-image-link">
                      <img id="main-image" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>"/>
                    </a>
                  </div>
                <?php } ?>
                <?php if ($images) { ?>
                  <div class="thumbnails__list-image owl-carousel" id="image-additional">
                    <?php foreach ($images as $image) { ?>
                      <div>
                        <a title="<?php echo $heading_title; ?>" class="product-image-link" href="<?php echo $image['popup']; ?>" data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>">
                          <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                        </a>
                      </div>
                    <?php } ?>
                  </div>
                <?php } ?>
              </ul>
            <?php } ?>
          </div>
          <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-lg-6 col-sm-6'; ?>
          <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-lg-7 col-sm-7'; ?>
          <?php } else { ?>
            <?php $class = 'col-lg-7 col-sm-7'; ?>
          <?php } ?>
          <div class="<?php echo $class; ?> product-info">
              
              
            <h1><?php echo $heading_title; ?></h1>
            <?php if ($review_status) { ?>
              <div class="product-rating">
                <p>
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star product-rate fa-stack-1x"></i></span>
                    <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star product-rate fa-stack-1x"></i><i class="fa fa-star product-rated fa-stack-1x"></i></span>
                    <?php } ?>
                  <?php } ?>
                  <a href="" class="write-review" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews ."  " . "/"; ?></a>
                  <a href="" class="write-review" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
                </p>
              </div>
            <?php } ?>
            <?php if ($price) { ?>
              <ul class="list-unstyled product-price">
                <?php if (!$special) { ?>
                  <li>
                    <span><?php echo $price; ?></span>
                  </li>
                <?php } else { ?>
                  <li>
                    <span><?php echo $special; ?></span>
                  </li>
                  <li><span class="product-price--old"><?php echo $price; ?></span></li>
                <?php } ?>
              </ul>
              <?php if ($special) { ?>
                <?php if ($kuler->getSkinOption('show_save_percent')) { ?>
                  <div class="product-save-precent">
                    <?php echo $kuler->translate($kuler->getSkinOption('save_text')); ?> <span><?php echo $kuler->calculateSalePercent($special, $price); ?>%</span>
                  </div>
                <?php } ?>
              <?php } ?>
              <ul>
                <?php if ($points) { ?>
                  <li><?php echo $text_points; ?> <?php echo $points; ?></li>
                <?php } ?>
                <?php if ($discounts) { ?>
                  <li>
                    <hr>
                  </li>
                  <?php foreach ($discounts as $discount) { ?>
                    <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                  <?php } ?>
                <?php } ?>
              </ul>
            <?php } ?>
            <!--h3><?php echo $kuler->language->get('text_product_details'); ?></h3-->
            
            <ul class="list-unstyled product-options">
                <li>Front Neck - <span id="fn"></span></li>
                <li>Back Neck - <span id="bn"></span></li>
                <li>Sleeve Style - <span id="sl"></span></li>
                <li>Add-Ons - <span id="ao"></span></li>
            </ul>
            
            <ul class="list-unstyled product-options">
              <?php if ($manufacturer) { ?>
                <li>
                  <?php if ($kuler->getSkinOption('show_brand_logo')) { ?>
                    <a href="<?php echo $manufacturers; ?>">
                      <img src="<?php echo $kuler->getManufacturerImage($product_id); ?>" alt="<?php echo $manufacturer; ?>" />
                    </a>
                  <?php } else { ?>
                    <?php echo $text_manufacturer; ?>
                    <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a>
                  <?php } ?>
                </li>
              <?php } ?>
              <li><?php echo $text_model; ?> <?php echo $model; ?></li>
              <?php if ($reward) { ?>
                <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
              <?php } ?>
              <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
            </ul>
            <div id="product">
              <?php if ($options) { ?>
                <hr>
                <h3><?php echo $text_option; ?></h3>
                
                <div class="panel-group" id="accordion">
                <?php $f=0; foreach ($options as $option) {
                        $f++;
                    ?>
                    
                    <div class="panel panel-default">
                      <div class="panel-heading">
                          <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $option['product_option_id']; ?>">
                                  <?php echo $option['name']; ?></a>
                          </h4>
                      </div>
                      <div id="collapse<?php echo $option['product_option_id']; ?>" class="panel-collapse collapse <?php if($f==1){ ?>in<?php
                      
                      } ?>">
                          <div class="panel-body">
                            <?php if ($option['type'] == 'select') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                      <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                      <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                      </option>
                                        <?php } ?>
                                  </select>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'radio') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                      <div class="radio option-selection">
                                          <label>
                                              <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                              <?php echo $option_value['name']; ?>
                                              <?php if ($option_value['price']) { ?>
                                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                              <?php } ?>
                                          </label>
                                      </div>
                                        <?php } ?>
                                  </div>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'checkbox') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                      <div class="checkbox">
                                          <label>
                                              <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                              <?php echo $option_value['name']; ?>
                                              <?php if ($option_value['price']) { ?>
                                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                              <?php } ?>
                                          </label>
                                      </div>
                                        <?php } ?>
                                  </div>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'image') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                      <div class="radio option-selection">
                                          <label>
                                              <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                              <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                              <?php if ($option_value['price']) { ?>
                                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                              <?php } ?>
                                          </label>
                                      </div>
                                        <?php } ?>
                                  </div>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'text') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'textarea') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'file') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'date') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="input-group date">
                                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                      <span class="input-group-btn">
                                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                      </span></div>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'datetime') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="input-group datetime">
                                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                      <span class="input-group-btn">
                                          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                      </span></div>
                              </div>
                                  <?php } ?>
                                  <?php if ($option['type'] == 'time') { ?>
                              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="input-group time">
                                      <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                      <span class="input-group-btn">
                                          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                      </span></div>
                              </div>
                                  <?php } ?>                              
                          </div>
                      </div>
                    </div>                
                
                
                  
                <?php } ?>
              </div>      
              <?php } ?>
              
              <?php if ($recurrings) { ?>
                <hr>
                <h3><?php echo $text_payment_recurring ?></h3>
                <div class="form-group required">
                  <select name="recurring_id" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($recurrings as $recurring) { ?>
                      <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                    <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
                </div>
              <?php } ?>
              <?php if ($kuler->getSkinOption('show_custom_block')) { ?>
                <div class="custom-block"><?php echo $kuler->translate($kuler->getSkinOption('custom_block_content')); ?></div>
              <?php } ?>
              <div class="quantity-form form-group">
                <?php if (!$kuler->getSkinOption('show_number_quantity')) { ?>
                  <button type="button" id="qty-dec" class="quantity__button"><i class="fa fa-caret-left"></i></button>
                  <input type="text" name="quantity" size="2" class="dynamic-number" value="<?php echo $minimum; ?>" />
                  <button type="button" id="qty-inc" class="quantity__button"><i class="fa fa-caret-right"></i></button>
                <?php } else { ?>
                  <button type="button" id="qty-dec" class="quantity__button"><i class="fa fa-caret-left"></i></button>
                  <input type="text" name="quantity" size="2" class="dynamic-number" value="<?php echo $minimum; ?>" data-min="<?php echo $minimum; ?>" data-dec="#qty-dec" data-inc="#qty-inc"/>
                  <button type="button" id="qty-inc" class="quantity__button"><i class="fa fa-caret-right"></i></button>
                <?php } ?>
                <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
              </div>
              <div class="product-detail button-group">
                <div class="product-detail__group-buttons">
                  <button id="button-cart" class="product-detail-button product-detail-button--cart" type="button" data-loading-text="<?php echo $text_loading; ?>">
                    <span><?php echo $button_cart; ?></span>
                  </button>
                  <button class="product-detail-button product-detail-button--wishlist" type="button" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');">

                  </button>
                  <button class="product-detail-button product-detail-button--compare" type="button" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');">

                  </button>
                </div>
              </div>
              <?php if ($tags) { ?>
                <p class="tag"><span class="tag__title"><?php echo $text_tags; ?></span>
                  <?php for ($i = 0; $i < count($tags); $i++) { ?>
                    <?php if ($i < (count($tags) - 1)) { ?>
                      <a class="tag__name" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                    <?php } else { ?>
                      <a class="tag__name" href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                    <?php } ?>
                  <?php } ?>
                  .
                </p>
              <?php } ?>
              <?php if ($kuler->getSkinOption('default_sharing')) { ?>
                <div class="product-share"><!-- AddThis Button BEGIN -->
                                           <!-- AddThis Button BEGIN -->
                  <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
                                           <!-- AddThis Button END -->
                </div>
              <?php } ?>
              <?php if ($minimum > 1) { ?>
                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="product-tabs">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                <?php if ($attribute_groups) { ?>
                  <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                <?php } ?>
                <?php if ($review_status) { ?>
                  <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                <?php } ?>
                <?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
                  <li><a data-toggle="tab" href="#tab-custom-tab-1"><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_title')); ?></a></li>
                <?php } ?>
                <?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
                  <li><a data-toggle="tab" href="#tab-custom-tab-2"><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_title')); ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
                <?php if ($attribute_groups) { ?>
                  <div class="tab-pane" id="tab-specification">
                    <table class="table table-bordered">
                      <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <thead>
                        <tr>
                          <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                          <tr>
                            <td><?php echo $attribute['name']; ?></td>
                            <td><?php echo $attribute['text']; ?></td>
                          </tr>
                        <?php } ?>
                        </tbody>
                      <?php } ?>
                    </table>
                  </div>
                <?php } ?>
                <?php if ($review_status) { ?>
                  <div class="tab-pane" id="tab-review">
                    <form class="form-horizontal">
                      <div id="review"></div>
                      <h2><?php echo $text_write; ?></h2>
                      <div class="form-group required">
                        <div class="col-lg-12 col-sm-12">
                          <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                          <input type="text" name="name" value="" id="input-name" class="form-control" />
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-lg-12 col-sm-12">
                          <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                          <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                          <div class="help-block"><?php echo $text_note; ?></div>
                        </div>
                      </div>
                      <div class="form-group required">
                        <div class="col-lg-12 col-sm-12">
                          <label class="control-label"><?php echo $entry_rating; ?></label>
                          &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                          <input type="radio" name="rating" value="1" />
                          &nbsp;
                          <input type="radio" name="rating" value="2" />
                          &nbsp;
                          <input type="radio" name="rating" value="3" />
                          &nbsp;
                          <input type="radio" name="rating" value="4" />
                          &nbsp;
                          <input type="radio" name="rating" value="5" />
                          &nbsp;<?php echo $entry_good; ?></div>
                      </div>
                      <?php if ($site_key) { ?>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                          </div>
                        </div>
                      <?php } ?>
                      <div class="buttons">
                        <div class="pull-right">
                          <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                        </div>
                      </div>
                    </form>
                  </div>
                <?php } ?>
                <?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
                  <div id="tab-custom-tab-1" class="tab-pane">
                    <?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_content')); ?>
                  </div>
                <?php } ?>
                <?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
                  <div id="tab-custom-tab-2" class="tab-pane">
                    <?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_content')); ?>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php if ($products && $kuler->getSkinOption('show_related_products')) { ?>
          <div class="product-related">
            <div class="box-heading">
              <span><?php echo $kuler->language->get('text_related_products'); ?></span>
            </div>
            <div id="product-related" class="row owl-carousel">
              <?php foreach ($products as $product) { ?>
                <div class="product-layout product-grid col-md-3 col-lg-12 col-xs-12">
	                <div class="product-wrapper">
		                <?php if ($product['thumb']) { ?>
			                <div class="product-thumb">
				                <div class="product-thumb__primary">
					                <a href="<?php echo $product['href']; ?>">
						                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
					                </a>
				                </div>
				                <?php if ($images = $kuler->getProductImages($product['product_id'])) { ?>
					                <?php if(!$kuler->mobile->isMobile() && $kuler->getSkinOption('enable_swap_image')){ ?>
						                <?php $size = $kuler->getImageSizeByPath($product['thumb']); ?>
						                <div class="product-thumb__secondary hidden-xs hidden-sm hidden-md">
							                <a href="<?php echo $product['href']; ?>">
								                <img src="<?php echo $kuler->resizeImage($images[0], $size['width'], $size['height']); ?>" alt="<?php echo $product['name']; ?>"/>
							                </a>
						                </div>
					                <?php } ?>
				                <?php } //end swap image ?>
				                <div class="product-detail__row">
					                <?php if (Kuler::getInstance()->getSkinOption('show_quick_view')) { ?>
						                <a href="<?php echo Kuler::getInstance()->getQuickViewUrl($product); ?>" class="product-detail-button product-detail-button--quick-view" data-toggle="tooltip" title="<?php echo $kuler->translate($kuler->getSkinOption('view_button_text'));?>">
						                </a>
					                <?php } ?>
					                <button class="product-detail-button product-detail-button--compare" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
					                </button>
					                <button class="product-detail-button product-detail-button--wishlist" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
					                </button>
					                <button class="product-detail-button--cart" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
						                <span><?php echo $button_cart; ?></span>
					                </button>
				                </div>
				                <?php if ($product['special']) { ?>
					                <div class="product-sale">
						                <span>-<?php echo $kuler->calculateSalePercent($product['special'], $product['price']); ?>%</span>
					                </div><!--/.product-sale-->
				                <?php } //end special ?>
				                <?php if(isset($setting['deal_date']) && $setting['deal_date']) { ?>
					                <?php if(isset($product['date_end'])) { ?>
						                <?php
						                $parts = array('0000', '00', '00');

						                if ($product['date_end']) {
							                $parts = explode('-', $product['date_end']);
						                }
						                ?>
						                <div class="product-deal-countdown" data-is-deal="<?php echo $product['date_end'] ? 'true' : 'false' ?>" data-product-id="<?php echo $product['product_id'] ?>" data-date-end="<?php echo $product['date_end'] ?>" data-year="<?php echo $parts[0] ?>" data-month="<?php echo $parts[1] ?>" data-day="<?php echo $parts[2] ?>"></div>
					                <?php }  ?>
				                <?php } //end deal date ?>
			                </div><!--/.produc-thumb-->
		                <?php } else { ?>
			                <div class="product-thumb product-thumb--no-image">
				                <a href="<?php echo $product['href']; ?>">
					                <img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" />
				                </a>
			                </div><!--/.product-thumb--no-image-->
		                <?php } //end product thumb ?>
		                <h4 class="product-name">
			                <a href="<?php echo $product['href']; ?>">
				                <?php echo $product['name']; ?>
			                </a>
		                </h4>
		                <p class="product-price">
			                <?php if (!$product['special']) { ?>
				                <?php echo $product['price']; ?>
			                <?php } else { ?>
				                <span class="product-price--new"><?php echo $product['special']; ?></span>
				                <span class="product-price--old"><?php echo $product['price']; ?></span>
			                <?php } ?>
		                </p>
		                <div class="product-rating">
			                <?php for ($i = 1; $i <= 5; $i++) { ?>
				                <?php if ($product['rating'] < $i) { ?>
					                <span class="fa fa-stack"><i class="fa fa-star product-rate fa-stack-1x"></i></span>
				                <?php } else { ?>
					                <span class="fa fa-stack"><i class="fa fa-star product-rate fa-stack-1x"></i><i class="fa fa-star product-rated fa-stack-1x"></i></span>
				                <?php } ?>
			                <?php } ?>
		                </div>
		                <div class="product-description hidden">
			                <?php echo $product['description']; ?>
		                </div>
	                </div>
                </div>
              <?php } ?>
            </div>
            <div class="nav">
              <span class="prev">&lt;</span>
              <span class="next">&gt;</span>
            </div>
            <script type="text/javascript">
              $(function () {
                var owl = $('.product-related .owl-carousel');
                owl.owlCarousel({
                  loop:true,
                  margin:10,
                  autoPlay: 9000,
                  dots: false,
                  navigation: false,
                  responsive:{
                    0:{
                      items:1
                    },
                    600:{
                      items:2
                    },
                    1000:{
                      items:4
                    }
                  }
                });

                $(".product-related .next").click(function(){
                  owl.trigger('next.owl.carousel');
                });

                $(".product-related .prev").click(function(){
                  owl.trigger('prev.owl.carousel');
                });
              });
            </script>
          </div>
        <?php } ?>
        <?php echo $content_bottom; ?></div>
      <?php echo $column_right; ?></div>
  </div>
  <script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
      $.ajax({
        url: 'index.php?route=product/product/getRecurringDescription',
        type: 'post',
        data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
        dataType: 'json',
        beforeSend: function() {
          $('#recurring-description').html('');
        },
        success: function(json) {
          $('.alert, .text-danger').remove();

          if (json['success']) {
            $('#recurring-description').html(json['success']);
          }
        }
      });
    });
    //--></script>
  <script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
      $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
        dataType: 'json',
        beforeSend: function() {
          $('#button-cart').button('loading');
        },
        complete: function() {
          $('#button-cart').button('reset');
        },
        success: function(json) {
          $('.alert, .text-danger').remove();
          $('.form-group').removeClass('has-error');

          if (json['error']) {
            if (json['error']['option']) {
              for (i in json['error']['option']) {
                var element = $('#input-option' + i.replace('_', '-'));

                if (element.parent().hasClass('input-group')) {
                  element.parent().after('<div class="alert alert-danger">' + json['error']['option'][i] + '</div>');
                } else {
                  element.after('<div class="alert alert-danger">' + json['error']['option'][i] + '</div>');
                }
              }
            }

            if (json['error']['recurring']) {
              $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
            }

            // Highlight any found errors
            $('.text-danger').parent().addClass('has-error');
          }

          if (json['success']) {
            kulerAlert('success', json['success']);
            Kuler.cart_product_total += parseInt($('[name=quantity]').val());

            $('.cart-product-total-number').text(Kuler.cart_product_total);

            $('#cart > ul').load('index.php?route=common/cart/info ul li');
          }
        }
      });
    });
    //--></script>
  <script type="text/javascript"><!--
    $('.date').datetimepicker({
      pickTime: false
    });

    $('.datetime').datetimepicker({
      pickDate: true,
      pickTime: true
    });

    $('.time').datetimepicker({
      pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function() {
      var node = this;

      $('#form-upload').remove();

      $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

      $('#form-upload input[name=\'file\']').trigger('click');

      if (typeof timer != 'undefined') {
        clearInterval(timer);
      }

      timer = setInterval(function() {
        if ($('#form-upload input[name=\'file\']').val() != '') {
          clearInterval(timer);

          $.ajax({
            url: 'index.php?route=tool/upload',
            type: 'post',
            dataType: 'json',
            data: new FormData($('#form-upload')[0]),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
              $(node).button('loading');
            },
            complete: function() {
              $(node).button('reset');
            },
            success: function(json) {
              $('.text-danger').remove();

              if (json['error']) {
                $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
              }

              if (json['success']) {
                alert(json['success']);

                $(node).parent().find('input').attr('value', json['code']);
              }
            },
            error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
          });
        }
      }, 500);
    });
    //--></script>
  <script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
      e.preventDefault();

      $('#review').fadeOut('slow');

      $('#review').load(this.href);

      $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
      $.ajax({
        url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
        type: 'post',
        dataType: 'json',
        data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : ''),
        beforeSend: function() {
          $('#button-review').button('loading');
        },
        complete: function() {
          $('#button-review').button('reset');
        },
        success: function(json) {
          $('.alert-success, .alert-danger').remove();

          if (json['error']) {
            $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
          }

          if (json['success']) {
            $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

            $('input[name=\'name\']').val('');
            $('textarea[name=\'text\']').val('');
            $('input[name=\'rating\']:checked').prop('checked', false);
          }
        }
      });
    });
    //-->
        
$(document).ready(function(){
    $(".option-selection > label").on('click',function(e){
        $(this).unbind('click');
        var cur_panel = $(this).parents(".collapse");    
        $(this).parents(".collapse").collapse('toggle');
        console.log(cur_panel.parent().next().find(".collapse").collapse("show"));
        
    });     
});



  
  
  
  
  </script>
<?php echo $footer; ?>