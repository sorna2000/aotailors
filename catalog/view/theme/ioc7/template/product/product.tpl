<?php
$kuler = Kuler::getInstance();
$kuler->language->load('kuler/ioc7');
$theme = $kuler->getTheme();
$kuler->addScript(array(
	"catalog/view/theme/$theme/js/lib/jquery.elevatezoom.js",
	"catalog/view/theme/$theme/js/product.js"
), true);

global $config;
?>
<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-lg-6 col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-lg-9 col-md-9'; ?>
    <?php } else { ?>
      <?php $class = 'col-lg-12 col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <div class="row">
        <!--div class="col-sm-5">
          <?php if ($thumb || $images) { ?>
            <div class="thumbnails">
              <?php if ($thumb) { ?>
                <div class="thumbnails__big-image" style="position: relative; max-width: <?php echo $config->get('config_image_thumb_width'); ?>px; max-height: <?php echo $config->get('config_image_thumb_height'); ?>px;">
                  <a href="<?php echo $popup; ?>" class="product-image-link">
                    <img id="main-image" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>"/>
                  </a>
                </div>
              <?php } ?>
              <?php if ($images) { ?>
                <div class="thumbnails__list-image owl-carousel" id="image-additional">
                  <?php foreach ($images as $image) { ?>
                    <div>
                      <a title="<?php echo $heading_title; ?>" class="product-image-link" href="<?php echo $image['popup']; ?>" data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>">
                        <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                      </a>
                    </div>
                  <?php } ?>
                </div>
                <script type="text/javascript">
                  $(function() {
                    $('.thumbnails__list-image').owlCarousel({
                      dots: false,
                      loop:false,
                      margin:5,
                      responsive:{
                        0:{
                          items:2
                        },
                        600:{
                          items:3
                        },
                        1000:{
                          items:5
                        }
                      }
                    });
                  });
                </script>
              <?php } ?>
            </div>
          <?php } ?>
        </div-->
        <div class="col-sm-12 product-info">
          <?php if ($special) { ?>
          <?php if ($kuler->getSkinOption('show_save_percent')) { ?>
          <div class="product-save-precent">
            <span><?php echo $kuler->translate($kuler->getSkinOption('save_text')); ?></span> <?php echo $kuler->calculateSalePercent($special, $price); ?>%
          </div>
          <?php } ?>
          <?php } ?>
          <h1 class="product-name"><?php echo $heading_title; ?></h1>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special) { ?>
            <li class="product-price" id="price_text"><?php echo $price; ?></li>
            <?php } else { ?>
            <li class="product-price">
              <span class="product-price--new" id="price_text"><?php echo $special; ?></span>
              <span class="product-price--old"><?php echo $price; ?></span>
            </li>
            
            <?php if ($kuler->getSkinOption('show_save_percent')) { ?>
            <li class="product-save-precent">
              <span><?php echo $kuler->translate($kuler->getSkinOption('save_text')); ?></span> <?php echo $kuler->calculateSalePercent($special, $price); ?>%
            </li>
            <?php } ?>
            <?php } ?>
            
            <input type="hidden" name="sprice" id="sprice" value="<?=$sprice?>"/>
            
            <?php if ($tax && $kuler->getSkinOption('show_tax')) { ?>
            <li><span class="product-price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span></li>
            <?php } ?>
            <?php if ($points) { ?>
            <!--li><?php echo $text_points; ?> <?php echo $points; ?></li-->
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          
          
            <div class="box-content clearafter">
               <div class="top-banner">
                  <div class="row">
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="top-banner__box top-banner__box--orange">
                           <svg class="top-banner__icon" viewBox="0 0 74 74" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="74px" height="74px">
                              <g>
                                 <g>
                                    <path d="M 67.7723 74 L 6.2277 74 C 2.794 74 0 71.2061 0 67.7722 L 0 6.2278 C 0 2.7932 2.794 0 6.2277 0 L 67.7723 0 C 71.2059 0 74 2.7932 74 6.2278 L 74 67.7722 C 74 71.2061 71.2059 74 67.7723 74 ZM 6.2277 3.6633 C 4.814 3.6633 3.6634 4.814 3.6634 6.2278 L 3.6634 67.7722 C 3.6634 69.186 4.814 70.3367 6.2277 70.3367 L 67.7723 70.3367 C 69.186 70.3367 70.3366 69.186 70.3366 67.7722 L 70.3366 6.2278 C 70.3366 4.814 69.186 3.6633 67.7723 3.6633 L 6.2277 3.6633 Z" fill="#ffffff"></path>
                                    <path d="M 38.8317 72.1877 L 35.1683 72.1877 L 35.1683 1.8105 L 38.8317 1.8105 L 38.8317 72.1877 Z" fill="#ffffff"></path>
                                    <path d="M 72.1891 38.8318 L 1.8116 38.8318 L 1.8116 35.1685 L 72.1891 35.1685 L 72.1891 38.8318 Z" fill="#ffffff"></path>
                                    <path d="M 26.8521 58.0352 C 23.9435 58.0352 21.2096 56.9031 19.1539 54.8472 C 14.9082 50.6018 14.9082 43.6934 19.1539 39.448 C 23.1014 35.4998 35.5632 35.1914 36.9714 35.1689 L 38.861 35.1392 L 38.8317 37.0295 C 38.8094 38.4375 38.5004 50.9001 34.5522 54.8469 C 32.4952 56.9031 29.7606 58.0352 26.8521 58.0352 ZM 35.0983 38.9016 C 30.2908 39.1628 23.6609 40.1211 21.7441 42.0381 C 18.9272 44.8557 18.9272 49.4392 21.7441 52.2571 C 23.1078 53.6211 24.9216 54.3718 26.8521 54.3718 C 28.7825 54.3718 30.597 53.6201 31.9622 52.2556 C 33.879 50.3396 34.837 43.7097 35.0983 38.9016 Z" fill="#ffffff"></path>
                                    <path d="M 35.1383 38.8616 L 35.1683 36.9712 C 35.1913 35.563 35.501 23.1001 39.4492 19.1521 C 41.5048 17.0962 44.2386 15.9631 47.1472 15.9631 C 50.0558 15.9631 52.7904 17.0962 54.8475 19.1516 C 59.0918 23.3972 59.0918 30.3049 54.8483 34.5508 C 50.8993 38.4993 38.4374 38.8093 37.0293 38.8318 L 35.1383 38.8616 ZM 47.1472 19.6265 C 45.2175 19.6265 43.4037 20.3774 42.0393 21.7422 C 40.1189 23.6628 39.1608 30.2913 38.9011 35.0984 C 43.7092 34.8364 50.3398 33.8774 52.2573 31.9602 C 55.0735 29.1433 55.0735 24.5591 52.2573 21.7422 C 50.8922 20.3782 49.0776 19.6265 47.1472 19.6265 Z" fill="#ffffff"></path>
                                    <path d="M 37 38.8318 C 36.5314 38.8318 36.0627 38.6531 35.705 38.2952 L 26.1029 28.6921 C 25.3875 27.9773 25.3875 26.8171 26.1029 26.1021 C 26.8184 25.3872 27.9775 25.3865 28.6931 26.1021 L 38.295 35.7051 C 39.0105 36.4202 39.0105 37.5806 38.295 38.2957 C 37.9373 38.6531 37.4686 38.8318 37 38.8318 Z" fill="#ffffff"></path>
                                    <path d="M 46.2042 48.0361 C 45.7356 48.0361 45.2668 47.8572 44.9092 47.4995 L 35.705 38.2952 C 34.9895 37.5806 34.9895 36.4202 35.705 35.7051 C 36.4204 34.9897 37.5796 34.9897 38.295 35.7051 L 47.4993 44.9092 C 48.2148 45.624 48.2148 46.7839 47.4993 47.4995 C 47.1415 47.8572 46.6729 48.0361 46.2042 48.0361 Z" fill="#ffffff"></path>
                                 </g>
                              </g>
                           </svg>
                           <div class="top-banner__text">
                              <h3 class="top-banner__title_prod">Choose Design & Book for Stitch</h3>
                              <!--p class="top-banner__subtitle">Order Online for your stitching in just few clicks and share your address</p-->
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="top-banner__box top-banner__box--yellow">
                           <svg class="top-banner__icon" viewBox="0 0 102 74" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="102px" height="74px">
                              <g>
                                 <path d="M 17.4518 34.6211 L 17.4518 32.4412 L 17.4493 34.6211 C 7.8278 34.6211 0 26.855 0 17.3101 C 0 7.7654 7.8273 0 17.4484 0 C 27.0687 0 34.896 7.7654 34.896 17.3101 C 34.896 26.855 27.0704 34.6211 17.4518 34.6211 ZM 17.4484 4.3596 C 10.2502 4.3596 4.3943 10.1692 4.3943 17.3101 C 4.3943 24.4514 10.2506 30.261 17.4493 30.261 L 17.4518 30.261 C 24.6475 30.261 30.5017 24.4514 30.5017 17.3101 C 30.5017 10.1692 24.6458 4.3596 17.4484 4.3596 Z" fill="#ffffff"></path>
                                 <path d="M 17.4484 20.3345 C 16.8889 20.3345 16.3292 20.1243 15.9006 19.7019 C 15.0393 18.8538 15.0342 17.4739 15.889 16.6196 L 22.5173 9.9934 C 23.3713 9.1384 24.7634 9.1343 25.6242 9.9814 C 26.4859 10.8296 26.491 12.2097 25.6362 13.0637 L 19.0079 19.6904 C 18.5787 20.1194 18.0132 20.3345 17.4484 20.3345 Z" fill="#ffffff"></path>
                                 <path d="M 17.4476 20.3345 C 16.9532 20.3345 16.4572 20.1702 16.0469 19.8335 L 10.9501 15.6445 C 10.0154 14.8765 9.8854 13.5022 10.6596 12.5759 C 11.4337 11.6487 12.819 11.5193 13.7532 12.2871 L 18.8499 16.4766 C 19.7845 17.2444 19.9142 18.6184 19.1401 19.5452 C 18.7057 20.0654 18.0793 20.3345 17.4476 20.3345 Z" fill="#ffffff"></path>
                                 <path d="M 30.0632 74 C 24.2552 74 19.5297 69.3118 19.5297 63.5486 C 19.5297 57.7861 24.2552 53.0994 30.0632 53.0994 C 35.871 53.0994 40.5966 57.7861 40.5966 63.5486 C 40.5966 69.3118 35.871 74 30.0632 74 ZM 30.0632 57.4587 C 26.6782 57.4587 23.924 60.1907 23.924 63.5486 C 23.924 66.9077 26.6782 69.6404 30.0632 69.6404 C 33.4481 69.6404 36.2023 66.9077 36.2023 63.5486 C 36.2023 60.1907 33.4481 57.4587 30.0632 57.4587 Z" fill="#ffffff"></path>
                                 <path d="M 84.2554 74 C 78.4485 74 73.7238 69.3118 73.7238 63.5486 C 73.7238 57.7861 78.4485 53.0994 84.2554 53.0994 C 90.0634 53.0994 94.7889 57.7861 94.7889 63.5486 C 94.7889 69.3118 90.0634 74 84.2554 74 ZM 84.2554 57.4587 C 80.8713 57.4587 78.118 60.1907 78.118 63.5486 C 78.118 66.9077 80.8713 69.6404 84.2554 69.6404 C 87.6404 69.6404 90.3946 66.9077 90.3946 63.5486 C 90.3946 60.1907 87.6404 57.4587 84.2554 57.4587 Z" fill="#ffffff"></path>
                                 <path d="M 97.8091 65.7283 L 92.5917 65.7283 C 91.3782 65.7283 90.3946 64.7524 90.3946 63.5486 C 90.3946 62.345 91.3782 61.3689 92.5917 61.3689 L 97.6058 61.3689 L 97.6058 57.4036 L 15.4144 57.4036 C 14.2007 57.4036 13.2172 56.4277 13.2172 55.2236 C 13.2172 54.0198 14.2007 53.0439 15.4144 53.0439 L 102 53.0439 L 102 61.5715 C 102 63.8635 100.1195 65.7283 97.8091 65.7283 Z" fill="#ffffff"></path>
                                 <path d="M 75.9209 65.7283 L 38.4012 65.7283 C 37.1876 65.7283 36.204 64.7524 36.204 63.5486 C 36.204 62.345 37.1876 61.3689 38.4012 61.3689 L 75.9209 61.3689 C 77.1345 61.3689 78.118 62.345 78.118 63.5486 C 78.118 64.7524 77.1345 65.7283 75.9209 65.7283 Z" fill="#ffffff"></path>
                                 <path d="M 21.7252 65.7283 L 15.4144 65.7283 C 14.2007 65.7283 13.2172 64.7524 13.2172 63.5486 C 13.2172 62.345 14.2007 61.3689 15.4144 61.3689 L 21.7252 61.3689 C 22.9387 61.3689 23.9223 62.345 23.9223 63.5486 C 23.9223 64.7524 22.9387 65.7283 21.7252 65.7283 Z" fill="#ffffff"></path>
                                 <path d="M 70.2495 57.4036 L 15.4144 57.4036 C 14.2007 57.4036 13.2172 56.4277 13.2172 55.2236 L 13.2172 40.2156 C 13.2172 39.0117 14.2007 38.0359 15.4144 38.0359 C 16.6279 38.0359 17.6115 39.0117 17.6115 40.2156 L 17.6115 53.0439 L 68.0523 53.0439 L 68.0523 17.5225 L 40.4464 17.5225 C 39.2328 17.5225 38.2492 16.5466 38.2492 15.3428 C 38.2492 14.1387 39.2328 13.1628 40.4464 13.1628 L 70.2495 13.1628 C 71.4631 13.1628 72.4467 14.1387 72.4467 15.3428 L 72.4467 55.2236 C 72.4467 56.4277 71.4631 57.4036 70.2495 57.4036 Z" fill="#ffffff"></path>
                                 <path d="M 99.8029 57.4036 L 70.2495 57.4036 C 69.0359 57.4036 68.0523 56.4277 68.0523 55.2236 L 68.0523 24.8486 C 68.0523 23.645 69.0359 22.6687 70.2495 22.6687 L 83.8564 22.6687 C 84.6683 22.6687 85.4141 23.1133 85.7961 23.8242 L 92.7188 36.7263 L 100.5916 39.7307 C 101.4404 40.0552 102 40.864 102 41.7656 L 102 55.2236 C 102 56.4277 101.0164 57.4036 99.8029 57.4036 ZM 72.4467 53.0439 L 97.6058 53.0439 L 97.6058 43.2627 L 90.3757 40.5029 C 89.8823 40.3145 89.4736 39.9563 89.2248 39.4932 L 82.5372 27.0283 L 72.4467 27.0283 L 72.4467 53.0439 Z" fill="#ffffff"></path>
                              </g>
                           </svg>
                           <div class="top-banner__text">
                              <h3 class="top-banner__title_prod">Pickup @ DoorStep&nbsp;</h3>
                              <!--p class="top-banner__subtitle">We Pickup your Material and Measurement Garment from your preferred location. Office or House</p-->
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="top-banner__box top-banner__box--green">
                           &nbsp;<img src="image/catalog/tailor-machine.png" class="img-rounded">              
                           <div style="padding-top: 30px;" class="top-banner__text">
                              <h3 class="top-banner__title_prod">Stiching By Professionals</h3>
                              <!--p class="top-banner__subtitle">Our professional tailors make sure that your dress is getting ready at perfect measurement</p-->
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="top-banner__box top-banner__box--blue">
                           <svg class="top-banner__icon" viewBox="0 0 58 74" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="58px" height="74px">
                              <g>
                                 <path d="M 29 74 C 28.8345 74 28.6697 73.9775 28.509 73.9331 C 28.2238 73.854 0 65.6992 0 37.9094 L 0 11.1819 C 0 10.6934 0.1957 10.2246 0.5425 9.8809 C 0.8901 9.5366 1.3582 9.3474 1.8499 9.3489 L 2.0033 9.3496 C 3.2454 9.3496 9.762 9.291 16.0327 8.1323 C 20.1978 7.3633 27.1846 5.5217 27.1846 1.833 C 27.1846 0.8213 28.0059 0 29.0193 0 C 30.0327 0 30.8542 0.8213 30.8542 1.833 C 30.8542 7.3088 46.2764 9.3496 55.9974 9.3496 L 56.1501 9.3489 C 56.667 9.3479 57.1099 9.5366 57.4575 9.8809 C 57.8043 10.2246 58 10.6934 58 11.1819 L 58 37.9094 C 58 65.6992 29.7762 73.854 29.491 73.9331 C 29.3303 73.9775 29.1655 74 29 74 ZM 3.6697 13.0005 L 3.6697 37.9094 C 3.6697 61.0244 25.4607 69.0923 29.0079 70.2505 C 32.7076 69.0813 54.3303 61.2434 54.3303 37.9094 L 54.3303 12.9934 C 51.6239 12.928 46.4038 12.678 41.3193 11.7371 C 35.308 10.625 31.196 8.8574 29.0178 6.4568 C 24.0488 11.9475 10.2701 12.8799 3.6697 13.0005 Z" fill="#ffffff"></path>
                                 <path d="M 23.1887 49.1494 L 13.9204 39.8904 C 13.2037 39.1746 13.2037 38.0137 13.9204 37.2976 C 14.6372 36.5823 15.7983 36.5823 16.5151 37.2976 L 23.1887 43.9648 L 41.4849 25.6863 C 42.2017 24.9709 43.3628 24.9709 44.0796 25.6863 C 44.7963 26.4023 44.7963 27.563 44.0796 28.2791 L 23.1887 49.1494 Z" fill="#ffffff"></path>
                              </g>
                           </svg>
                           <div class="top-banner__text">
                              <h3 class="top-banner__title_prod">Delivery in 7 Days</h3>
                              <!--p class="top-banner__subtitle">We deliver your custom stitched dress at your doorstep within 7 days from pickup</p-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>        
          

          <?php if ($review_status) { ?>
          <!--div class="product-review">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a id="write-review" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a>
            </p>
          </div-->
          <?php } ?>
          <h2 class="product-details-title"><?php echo $kuler->language->get('text_product_details'); ?></h2>
          <ul class="list-unstyled product-details">
            <?php if ($manufacturer) { ?>
            <li>
              <?php if ($kuler->getSkinOption('show_brand_logo')) { ?>
              <a href="<?php echo $manufacturers; ?>">
                <img src="<?php echo $kuler->getManufacturerImage($product_id); ?>" alt="<?php echo $manufacturer; ?>" />
              </a>
                  <?php } else { ?>
              <?php echo $text_manufacturer; ?>
              <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a>
              <?php } ?>
            </li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
          </ul>
          <div id="product" class="product-options">
            <?php if ($options) { ?>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option data-op="<?=$option_value['price_prefix'];?>" data-price="<?= $option_value['price_val'];?>" value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                  <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio  col-xs-5  col-md-4">
                  <label>
                    <input class="option-input radio" type="radio" name="option[<?php echo $option['product_option_id']; ?>]"  data-op="<?=$option_value['price_prefix'];?>" data-price="<?= $option_value['price_val'];?>"  value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox radio  col-xs-5  col-md-4">
                  <label>
                    <input type="checkbox" class="option-input"  data-op="<?=$option_value['price_prefix'];?>" data-price="<?= $option_value['price_val'];?>"  name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                    
                        <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' requireds' : ''); ?> op-container">
                <label class="control-label op-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>" class="row option-box">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio col-xs-5  col-md-4">
                  <label>
                      <input type="radio"  data-op="<?=$option_value['price_prefix'];?>" data-price="<?= $option_value['price_val'];?>"  class="option-input radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea class="form-control" name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['value']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" style="width:100%"></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> op-container">
              <label class="control-label op-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>

            <div class="form-group quantity">
              <label class="control-label quantity_label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <?php if (!$kuler->getSkinOption('show_number_quantity')) { ?>
              <input type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" class="form-control quantity__input" id="input-quantity"  />
              <?php } else { ?>
              <button type="button" id="qty-dec" class="quantity__button">-</button>
              <input type="text" name="quantity" size="2" id="input-quantity" class="form-control dynamic-number quantity__input" value="<?php echo $minimum; ?>" data-min="<?php echo $minimum; ?>" data-dec="#qty-dec" data-inc="#qty-inc" />
              <button type="button" id="qty-inc" class="quantity__button">+</button>
              <?php } ?>
              <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
            </div>
            <div class="product-detail">
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_cart; ?>" class="product-detail-button product-detail-button--cart">
                <?php echo $button_cart; ?>
              </button>
              <div class="product-detail__row">
                <button type="button" data-toggle="tooltip" class="col-xs-2 product-detail-button product-detail-button--wishlist" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');">
                  <?php echo $button_wishlist; ?>
                </button>
                <button type="button" data-toggle="tooltip" class="col-xs-2 product-detail-button product-detail-button--compare" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');">
                  <?php echo $button_compare; ?>
                </button>
              </div>
            </div>
            <?php if ($kuler->getSkinOption('default_sharing')) { ?>
            <div class="product-share"><!-- AddThis Button BEGIN -->
              <!-- AddThis Button BEGIN -->
              <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
              <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
              <!-- AddThis Button END -->
            </div>
            <?php } ?>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
            
            <?php if ($kuler->getSkinOption('show_custom_block')) { ?>
            <div class="custom-block"><?php echo $kuler->translate($kuler->getSkinOption('custom_block_content')); ?></div>
            <?php } ?>            
            
          </div>
        </div>
        <!--div class="col-lg-12">
          <div class="product-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-description" data-toggle="tab"><span><?php echo $tab_description; ?></span></a></li>
              <?php if ($attribute_groups) { ?>
                <li><a href="#tab-specification" data-toggle="tab"><span><?php echo $tab_attribute; ?></span></a></li>
              <?php } ?>
              <?php if ($review_status) { ?>
                <li><a href="#tab-review" data-toggle="tab" id="review-tab-title"><span><?php echo $tab_review; ?></span></a></li>
              <?php } ?>
              <?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
                <li><a data-toggle="tab" href="#tab-custom-tab-1"><span><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_title')); ?></span></a></li>
              <?php } ?>
              <?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
                <li><a data-toggle="tab" href="#tab-custom-tab-2"><span><?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_title')); ?></span></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
              <?php if ($attribute_groups) { ?>
                <div class="tab-pane" id="tab-specification">
                  <table class="table table-bordered">
                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                      <thead>
                      <tr>
                        <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                        <tr>
                          <td><?php echo $attribute['name']; ?></td>
                          <td><?php echo $attribute['text']; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    <?php } ?>
                  </table>
                </div>
              <?php } ?>
              <?php if ($review_status) { ?>
                <div class="tab-pane" id="tab-review">
                  <form class="form-horizontal">
                    <div id="review"></div>
                    <h2><?php echo $text_write; ?></h2>
                    <div class="form-group required">
                      <div class="col-lg-12 col-sm-12">
                        <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                        <input type="text" name="name" value="" id="input-name" class="form-control" />
                      </div>
                    </div>
                    <div class="form-group required">
                      <div class="col-lg-12 col-sm-12">
                        <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                        <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                        <div class="help-block"><?php echo $text_note; ?></div>
                      </div>
                    </div>
                    <div class="form-group required">
                      <div class="col-lg-12 col-sm-12">
                        <label class="control-label"><?php echo $entry_rating; ?></label>
                        &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                        <input type="radio" name="rating" value="1" />
                        &nbsp;
                        <input type="radio" name="rating" value="2" />
                        &nbsp;
                        <input type="radio" name="rating" value="3" />
                        &nbsp;
                        <input type="radio" name="rating" value="4" />
                        &nbsp;
                        <input type="radio" name="rating" value="5" />
                        &nbsp;<?php echo $entry_good; ?></div>
                    </div>
                    <div class="form-group required">
                      <div class="col-lg-12 col-sm-12">
                        <label class="control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
                        <input type="text" name="captcha" value="" id="input-captcha" class="form-control" />
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-12 col-sm-12"> <img src="index.php?route=tool/captcha" alt="" id="captcha" /> </div>
                    </div>
                    <div class="buttons pull-right">
                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn"><span><?php echo $button_continue; ?></span></button>
                    </div>
                  </form>
                </div>
              <?php } ?>
              <?php if ($kuler->getSkinOption('show_custom_tab_1')) { ?>
                <div id="tab-custom-tab-1" class="tab-pane">
                  <?php echo $kuler->translate($kuler->getSkinOption('custom_tab_1_content')); ?>
                </div>
              <?php } ?>
              <?php if ($kuler->getSkinOption('show_custom_tab_2')) { ?>
                <div id="tab-custom-tab-2" class="tab-pane">
                  <?php echo $kuler->translate($kuler->getSkinOption('custom_tab_2_content')); ?>
                </div>
              <?php } ?>
            </div>
          </div>
        </div-->
      </div>
      <?php if ($products && $kuler->getSkinOption('show_related_products')) { ?>
        <div class="product-related">
          <div class="box-heading">
            <span><?php echo $text_related; ?></span>
          </div>
          <div class="row">
            <?php foreach ($products as $product) { ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-lg-6 col-md-6 col-sm-6 col-xs-12'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = ' col-lg-4 col-md-6 col-sm-6 col-xs-12'; ?>
            <?php } else { ?>
            <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
            <?php } ?>
              <div class="product-layout product-grid <?php echo $class; ?>">
                <div class="product-wrapper">
                  <?php if ($product['thumb']) { ?>
                  <div class="product-thumb">
                    <div class="product-thumb__primary">
                      <a href="<?php echo $product['href']; ?>">
                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                      </a>
                    </div>
                    <?php if ($images = $kuler->getProductImages($product['product_id'])) { ?>
                    <?php if(!$kuler->mobile->isMobile() && $kuler->getSkinOption('enable_swap_image')){ ?>
                    <?php $size = $kuler->getImageSizeByPath($product['thumb']); ?>
                    <div class="product-thumb__secondary">
                      <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                      <img src="<?php echo $kuler->resizeImage($images[0], $size['width'], $size['height']); ?>" alt="<?php echo $product['name']; ?>"/>
                    </div>
                    <?php } ?>
                    <?php } //end swap image ?>
                    <?php if (Kuler::getInstance()->getSkinOption('show_quick_view')) { ?>
                    <a class="product-detail-button product-detail-button--quick-view" href="<?php echo Kuler::getInstance()->getQuickViewUrl($product); ?>" data-toggle="tooltip" title="<?php echo $kuler->translate($kuler->getSkinOption('view_button_text')); ?>"><?php echo $kuler->translate($kuler->getSkinOption('view_button_text')); ?></a>
                    <?php } //end show quick-view button ?>
                  </div><!--/.produc-thumb-->
                  <?php } else { ?>
                  <div class="product-thumb product-thumb--no-image">
                    <img src="image/no_image.png" alt="<?php echo $product['name']; ?>" />
                  </div><!--/.product-thumb--no-image-->
                  <?php } //end product thumb ?>
                  <div class="product-bottom-info">

                    <div class="product-name">
                      <a href="<?php echo $product['href']; ?>">
                        <?php echo $product['name']; ?>
                      </a>
                    </div>

                    <?php if($product['rating']) { ?>
                    <div class="product-rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } //end product rating ?>

                    <div class="product-description hidden">
                      <p><?php echo $product['description']; ?></p>
                    </div>

                    <div class="product-price">
                      <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                      <?php } else { ?>
                      <span class="product-price--new"><?php echo $product['special']; ?></span>
                      <span class="product-price--old"><?php echo $product['price']; ?></span>
                      <?php } ?>
                    </div>

                    <div class="product-detail button-group">
                      <button class="product-detail-button product-detail-button--cart" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                        <?php echo $button_cart; ?>
                      </button>
                      <div class="product-detail__row">
                        <button class="product-detail-button product-detail-button--wishlist" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                          <?php echo  $kuler->language->get('text_button_wishlist'); ?>
                        </button>
                        <button class="product-detail-button product-detail-button--compare" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                          <?php echo  $kuler->language->get('text_button_compare'); ?>
                        </button>
                      </div>
                    </div><!--./product-detail-->
                  </div>
                  <?php if ($product['special']) { ?>
                  <?php if ($kuler->getSkinOption('enable_swap_image')) { ?>
                  <div class="product-sale">
                    -<?php echo $kuler->calculateSalePercent($product['special'], $product['price']); ?>%
                  </div><!--/.product-sale-->
                  <?php } ?>
                  <?php } //end special ?>
                  <div class="product-right-info hidden">

                    <div class="product-name">
                      <a href="<?php echo $product['href']; ?>">
                        <?php echo $product['name']; ?>
                      </a>
                    </div>

                    <?php if($product['rating']) { ?>
                    <div class="product-rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } //end product rating ?>

                    <div class="product-description">
                      <p><?php echo $product['description']; ?></p>
                    </div>

                    <div class="product-detail button-group">
                      <button class="product-detail-button product-detail-button--cart" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                        <?php echo $button_cart; ?>
                      </button>
                      <div class="product-detail__row">
                        <button class="product-detail-button product-detail-button--wishlist" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                          <?php echo  $kuler->language->get('text_button_wishlist'); ?>
                        </button>
                        <button class="product-detail-button product-detail-button--compare" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                          <?php echo  $kuler->language->get('text_button_compare'); ?>
                        </button>
                      </div>
                    </div><!--./product-detail-->
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php if ($kuler->getSkinOption('related_products_style') == 'slider') { ?>
<script type="text/javascript"><!--
  $(function(){
    $('.product-related > .row').owlCarousel({
      stagePadding:30,
      dots: false,
      nav: true,
      autoHeight:true,
      items:1
    });
  });
  //--></script>
<?php } ?>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
						
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}
				
				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
				
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}
			
			if (json['success']) {
        kulerAlert('success', json['success']);
				
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				
				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

    $(' #product input[type=\'radio\'], #product input[type=\'checkbox\'], #product select').change(function(){
        var base_price =$('#sprice').val();
        var vprice =0;
        $('#product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select').each(function(){
            if($(this).is('input'))
            {   
                if($(this).attr('data-op')!='' && typeof $(this).attr('data-op')!= 'undefined') {
                    var s =$(this).attr('data-op') + $(this).attr('data-price');
                    vprice += eval(s); 
                }
            }
            else {
                var op = $(this).find("option:selected").attr('data-op');
                
                if(typeof op!= 'undefined'){
                    var pri = $(this).find("option:selected").attr('data-price');
                    var s = op + pri;
                    pf = parseFloat(s);
                    vprice += pf;
                }
            }
            
        })
        final_price = parseFloat(base_price) + parseFloat(vprice);
        $('#price_text').html("₹ "+ final_price);
        
    });        
    

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;
	
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
	
	$('#form-upload input[name=\'file\']').trigger('click');
	
	$('#form-upload input[name=\'file\']').on('change', function() {
		$.ajax({
			url: 'index.php?route=tool/upload',
			type: 'post',
			dataType: 'json',
			data: new FormData($(this).parent()[0]),
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$(node).button('loading');
			},
			complete: function() {
				$(node).button('reset');
			},
			success: function(json) {
				$('.text-danger').remove();
				
				if (json['error']) {
					$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
				}
				
				if (json['success']) {
					alert(json['success']);
					
					$(node).parent().find('input').attr('value', json['code']);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
			$('#captcha').attr('src', 'index.php?route=tool/captcha#'+new Date().getTime());
			$('input[name=\'captcha\']').val('');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();
			
			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});

//--></script> 
<?php echo $footer; ?>