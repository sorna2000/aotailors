<div id="carousel<?php echo $module; ?>" class="carousel-module" dir="ltr">
  <div class="owl-carousel">
    <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <div>
      <a href="<?php echo $banner['link']; ?>">
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
      </a>
    </div>
    <?php } else { ?>
    <div>
      <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    </div>
    <?php } ?>
    <?php } ?>
  </div>
</div>
<script type="text/javascript">
  $(function () {

    var owl = $('#carousel<?php echo $module; ?> .owl-carousel');

    owl.owlCarousel({
      loop:true,
      center:true,
      margin:5,
      dots: false,
      nav:true,
      responsive:{
        0:{
          items:2
        },
        600:{
          items:4
        },
        1000:{
          items:5
        }
      }
    });
  });
</script>