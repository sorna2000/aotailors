<?php echo $header; ?>
	<div class="container">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
		<div class="row">
			<?php echo $column_left; ?>
			<?php if ($column_left && $column_right) { ?>
				<?php $class = 'col-md-6 col-lg-6'; ?>
			<?php } elseif ($column_left || $column_right) { ?>
				<?php $class = 'col-md-9 col-lg-9'; ?>
			<?php } else { ?>
				<?php $class = 'col-md-12 col-lg-12'; ?>
			<?php } ?>
			<div id="content" class="<?php echo $class; ?>">
				<?php echo $content_top; ?>
				<h2><?php echo $category['name']; ?></h2>
				<div class="col-md-12">
					<div class="image"><img src="<?php echo $category['image_thumb']; ?>" alt="<?php echo $category['name']; ?>" /></div>
					<?php if ($category['description']) { ?>
						<p class="blog-description"> <?php echo $category['description']; ?> </p>
					<?php } ?>
				</div>
				<?php if ($sub_categories) { ?>
					<div class="sub-categories">
						<h4><?php echo _t('text_sub_categories'); ?></h4>
						<ul>
							<?php foreach ($sub_categories as $sub_category) { ?>
								<li><a href="<?php echo $sub_category['link']; ?>"><?php echo $sub_category['name']; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
				<div class="article-list clearafter">
					<?php foreach ($articles as $article) { ?>
						<?php if ($column==2) { ?>
							<?php $class = 'col-md-6'; ?>
						<?php } elseif ($column==3) { ?>
							<?php $class = 'col-md-4'; ?>
						<?php } elseif ($column==4) { ?>
							<?php $class = 'col-md-3'; ?>
						<?php } else { ?>
							<?php $class = 'col-md-12'; ?>
						<?php } ?>
						<div class="article <?php echo $class; ?> wow fadeInRight" data-wow-offset="100">
							<div class="col-sm-4 col-xs-12">
								<div class="article__image">
									<a href="<?php echo $article['link']; ?>" class="article-image">
										<img src="<?php echo $article['featured_image_thumb']; ?>" />
									</a>
								</div>
							</div>
							<div class="col-sm-8 col-xs-12 no-padding-lg no-padding-md no-padding-sm">
								<div class="article__header">
									<h2><a href="<?php echo $article['link']; ?>"><?php echo $article['name']; ?></a></h2>
									<div class="article__extra-info">
										<?php if ($article['display_author']) { ?>
										<?php echo '<span class="article__extra-info__author vcard">'; echo _t('text_by_x', '<a rel="author">'. $article['author_name'] .'</a></span>'); ?>
										<?php } ?>

										<?php if ($article['display_category'] && $article['categories']) { ?>
										<?php echo '<span class="article__extra-info__category">'; ?>
										<?php echo _t('text_in'); ?>
										<?php $article_links = array(); ?>
										<?php foreach ($article['categories'] as $article_category) {
													$article_links[] = sprintf('<a href="%s">%s</a>', $article_category['link'], $article_category['name']);
										} ?>
										<?php echo implode(', ', $article_links); ?>
										<?php echo '</span>'; ?>
										<?php } ?>

										<?php if ($article['display_date']) { ?>
										<span class="article__extra-info__date"><?php echo _t('text_on'); ?><time><?php echo $article['date_added_formatted'];?></time></span>
										<?php } ?>

										<?php if ($article['comment_total'] && $article['display_comment']) { ?>
										<span class="article__extra-info__comment-total"><a href="<?php echo $article['link']; ?>#comments"><?php echo _t('text_x_comments', $article['comment_total']); ?></a></span>
										<?php } ?>
									</div>
								</div>
								<div class="article__content">
									<div class="article__description">
										<?php echo $article['description']; ?>
									</div>
									<div class="article__read-more">
										<a class="read-more btn" href="<?php echo $article['link']; ?>">
											<span><?php echo _t('text_read_more'); ?></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="pagination"> <?php echo $pagination; ?> </div>
			</div>
			<?php echo $column_right; ?>
			<?php echo $content_bottom; ?>
		</div>
	</div>
<?php echo $footer; ?>