<?php
// HTTP
define('HTTP_SERVER', 'http://aotailors.is/admin/');
define('HTTP_CATALOG', 'http://aotailors.is/');

// HTTPS
define('HTTPS_SERVER', 'http://aotailors.is/admin/');
define('HTTPS_CATALOG', 'http://aotailors.is/');

// DIR
define('DIR_APPLICATION', 'G:\MyFiles\Sites\tailorshop/admin/');
define('DIR_SYSTEM', 'G:\MyFiles\Sites\tailorshop/system/');
define('DIR_LANGUAGE', 'G:\MyFiles\Sites\tailorshop/admin/language/');
define('DIR_TEMPLATE', 'G:\MyFiles\Sites\tailorshop/admin/view/template/');
define('DIR_CONFIG', 'G:\MyFiles\Sites\tailorshop/system/config/');
define('DIR_IMAGE', 'G:\MyFiles\Sites\tailorshop/image/');
define('DIR_CACHE', 'G:\MyFiles\Sites\tailorshop/system/cache/');
define('DIR_DOWNLOAD', 'G:\MyFiles\Sites\tailorshop/system/download/');
define('DIR_UPLOAD', 'G:\MyFiles\Sites\tailorshop/system/upload/');
define('DIR_LOGS', 'G:\MyFiles\Sites\tailorshop/system/logs/');
define('DIR_MODIFICATION', 'G:\MyFiles\Sites\tailorshop/system/modification/');
define('DIR_CATALOG', 'G:\MyFiles\Sites\tailorshop/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'tailorshop');
define('DB_PREFIX', 'oc_');
